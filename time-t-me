#!/bin/sh

set -e

libs=$(sed -n -e'/t64$/d;
	/Package: lib.*[0-9]\(c102\|c2\|g\|ldbl\)\?$/s/^Package: //p' \
	debian/control || true)

for lib in $libs; do
	for control in debian/control debian/control.in; do
		if [ -e "$control" ]; then
			sed_suffix=
			provides_cmd='\
Provides: \$\{t64:Provides\}'
			replaces_cmd="\\
Replaces: $lib"
			breaks_cmd="\\
Breaks: $lib (<< \${source:Version})"
			provides=$(sed -n "$control" \
			           -e"/Package: $lib$/,/^$/ { /Provides:/p }")
			replaces=$(sed -n "$control" \
			           -e"/Package: $lib$/,/^$/ { /Replaces:/p }")
			breaks=$(sed -n "$control" \
			         -e"/Package: $lib$/,/^$/ { /Breaks:/p }")
			if [ -n "$provides" ]; then
				provides_cmd=
				sed -i "$control" -e"/^Package: $lib$/,/^$/ {
					 /^Provides:/ s/$/, \\$\{t64:Provides\}/
					}"
			fi
			if [ -n "$replaces" ]; then
				replaces_cmd=
				sed -i "$control" -e"/^Package: $lib$/,/^$/ {
					 s/^Replaces:[[:space:]]\+/&$lib, /
					}"
			fi
			if [ -n "$breaks" ]; then
				breaks_cmd=
				sed -i "$control" -e"/^Package: $lib$/,/^$/ {
					 s/^Breaks:[[:space:]]\+/&$lib (<< \${source:Version}), /
					}"
			fi
			if [ -n "$provides_cmd$replaces_cmd$breaks_cmd" ]; then
				sed_suffix='a'
			fi
			sed -i -e"/^Package: $lib$/ {
				s/\(c102\|c2\|g\|ldbl\)\?$/t64/;
				$sed_suffix$provides_cmd$replaces_cmd$breaks_cmd
}" "$control"
		fi
	done
	newlib=$(echo "$lib" | sed -e's/\(c102\|c2\|g\|ldbl\)\?$/t64/')
	for file in debian/${lib}.*; do
		newfile="debian/$newlib.${file#*.}"
		mv "$file" "$newfile"
		case $file in
			*.symbols|*.lintian-overrides|*.shlibs)
				sed -i "$newfile" -e"s/$lib/$newlib/g"
				;;
		esac
	done
	echo "$newlib: package-name-doesnt-match-sonames $lib" \
		>> debian/${newlib}.lintian-overrides
done

cat <<EOF
debian/control updated.  You will need to add the following code to
debian/rules:

  include /usr/share/dpkg/pkg-info.mk
  include /usr/share/dpkg/architecture.mk

  override_dh_gencontrol:
  	if [ $\$DEB_HOST_ARCH_BITS != 32 ] || [ $\$DEB_HOST_ARCH = i386 ]; then\\
  		libs=$\$(sed -n -e'/^Package: lib.*t64$\$/ { s/Package: //; p}' \\
  			debian/control); \\
  		for lib in $\$libs; do\\
  			oldlib=$\${lib%t64};\\
  			echo "t64:Provides=$\$oldlib (= \$(DEB_VERSION))" \\
  				>> debian/$\$lib.substvars;\\
  		done;\\
  	fi
  	dh_gencontrol
EOF
